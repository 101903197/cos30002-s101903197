import sys
import os
import time
import copy



NL  = '\n'
TAB = '\t'



class GOAPState:

  def __init__(self, states):
    self.states = {}
    for state in states:
      self.set_state(state)

  

  def set_state(self, name, initial_value = False):
    self.states[name] = initial_value

  

  def preconditions_met(self, action):

    for precondition in action.preconditions:
      if self.states[precondition['State']] != precondition['Required']:
        return False
    return True

  

  def perform_action(self, action):

    for effect in action.effects:
      self.states[effect['State']] = effect['Result']

  

  def print_states(self):

    print(NL + 'Current states:' + NL)
    for state, value in self.states.items():
      print(TAB + '[' + ('*' if value else ' ') + '] ' + state)



class GOAPAction:

  def __init__(self, name, cost):

    self.name = name
    self.cost = cost
    self.preconditions = []
    self.effects = []  

  def setup_action(self, before, after, maintain_before_state = True):


    for state in before:
      self.add_precondition(state)
      if not maintain_before_state:
        self.add_effect(state, False)

    for state in after:
      self.add_precondition(state, False)
      self.add_effect(state)  

  def add_precondition(self, precondition, required_value = True):

    self.preconditions.append({
      'State'    : precondition,
      'Required' : required_value
    })  

  def add_effect(self, effect, result_value = True):

    self.effects.append({
      'State'  : effect,
      'Result' : result_value
    })



class GOAPAgent:

  def __init__(self):

    self.state = []
    self.actions = []
    self.cost_so_far = 0

    self.paths_evaluated = 0

  

  def get_possible_actions(self, state = None):

    if not state:
      state = self.state

    return list(filter(lambda action: state.preconditions_met(action), self.actions))

  

  def print_possible_actions(self):

    print(NL + 'Possible actions:' + NL)
    for action in self.get_possible_actions():
      print(TAB + action.name + ' (' + str(action.cost) + ')')

  

  def perform_action(self, action):

    print(NL + 'Performing action:' + NL + NL + TAB + action.name)
    self.state.perform_action(action)
    self.cost_so_far += action.cost

  

  def depth_first_search(
    self,
    goal_state,
    state          = None,
    sequence       = None,
    initial_action = None
  ):

    # Start by using the current state of the Agent
    if not state:
      state = copy.deepcopy(self.state)

    # The sequence represents the series of actions taken to reach
    # the current state, as well as the total cost
    if not sequence:
      sequence = {
        'Actions' : [],
        'Cost'    : 0
      }
      self.paths_evaluated = 0

    # Has an initial action been defined?
    if initial_action:
      # If so, start by performing this action
      sequence['Actions'].append(initial_action)
      sequence['Cost'] += initial_action.cost
      state.perform_action(initial_action)

    # Have we reached the goal state?
    if state.states[goal_state]:

      self.paths_evaluated += 1
      path = ''
      for action in sequence['Actions']:
        path += action.name + ' -> '

      if self.paths_evaluated % 100 == 0:
        cls()
        print('Evaluating sequence #' + str(self.paths_evaluated) + '...' + NL)
        print('Cost: ' + str(sequence['Cost']))
        print('Sequence: ' + path[:-3])

      # If so, return the sequence we used to reach this point
      return sequence

    # If not, get all possible actions
    possible_actions = self.get_possible_actions(state)

    lowest_cost_sequence = None

    for action in possible_actions:
      # Try all possible actions
      possible_sequence = self.depth_first_search(
        goal_state,
        copy.deepcopy(state),
        copy.deepcopy(sequence),
        action
      )
      # Find the action that produces the lowest-cost sequence
      if not lowest_cost_sequence or possible_sequence['Cost'] < lowest_cost_sequence['Cost']:
        lowest_cost_sequence = possible_sequence

    # Return this sequence
    return lowest_cost_sequence



def cls():
  # Clear the console and print a newline
  os.system('cls')
  print()



# Actions
sow_seeds = GOAPAction('Sow Seeds',100)
grow_fruits = GOAPAction('Grow Fruits',20)
make_fjam = GOAPAction('Make Fruit Jam', 20)
grow_veg = GOAPAction('Grow Veggies', 15)
boil_veg = GOAPAction('Boil the veggies', 15)
jam_donut = GOAPAction('Make Jam Donuts', 40)
fried_veg = GOAPAction('Make Fried Veggies', 30)

grow_fruits  .setup_action(['HasSeeds'], ['HasFruits'], False)
make_fjam .setup_action(['HasSeeds', 'HasFruits'], ['HasJam'],  False)
grow_veg .setup_action(['HasSeeds'], ['HasVegs'],  False)
boil_veg.setup_action(['HasSeeds', 'HasVegs'], ['HasBoilVegs'], False)


sow_seeds.setup_action([],['HasSeeds'])
jam_donut.setup_action(['HasSeeds', 'HasFruits', 'HasJam'], ['HasJamDonut'], False)
fried_veg.setup_action(['HasSeeds', 'HasVegs' ,'HasBoilVegs'], ['HasFriedVeg'], False)


agent = GOAPAgent()

agent.state = GOAPState([
  'HasSeeds',
  'HasJam',
  'HasFruits',
  'HasVegs',
  'HasJamDonut',
  'HasBoilVegs',
  'HasFriedVeg',

])

agent.actions = [
  sow_seeds,
  grow_fruits,
  make_fjam,
  grow_veg,
  boil_veg,
  jam_donut,
  fried_veg,

]
# Perform a depth-first search to find the optimal sequence of actions

goal_state = 'HasJamDonut'
sequence = agent.depth_first_search(goal_state)

# Display the results

cls()
print('Goal: ' + goal_state + NL)
for i in range(len(sequence['Actions'])):
  print(str(i+1) + '. ' + sequence['Actions'][i].name + ' (' + str(sequence['Actions'][i].cost) + ')')

print(NL + 'Total cost: ' + str(sequence['Cost']))

input(NL + 'Press any key to continue...')

# Perform the sequence of actions

for action in sequence['Actions']:

  cls()
  print('Goal: ' + goal_state)
  print('Cost: ' + str(agent.cost_so_far))

  agent.state.print_states()
  agent.print_possible_actions()
  input(NL + 'Press any key to continue...')

  agent.perform_action(action)
  input(NL + 'Press any key to continue...')

# Display the end states

cls()
print('Goal: ' + goal_state)
print('Cost: ' + str(agent.cost_so_far))

agent.state.print_states()
