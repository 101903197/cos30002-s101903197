from graphics      import egi
from path          import Path
from pyglet.window import key
from random        import random, randrange, uniform
from vector2d      import Vector2D, Point2D
from math          import radians, sin, cos

# ---

AGENT_MODES = {
    key._1: 'Hide',
    
}

# ---

class Agent(object):

    def __init__(self, world=None, scale=30.0, mode='none', is_hunter = False):
        self.world = world
        self.mode  = mode
        self.scale = scale
        self.mass=1.0
        
        self.pos     = Vector2D(randrange(self.world.cx), randrange(self.world.cy))
        self.vel     = Vector2D()
        self.accel = Vector2D()

        direction    = radians(random()*360)
        self.heading = Vector2D(sin(direction), cos(direction))
        self.side    = self.heading.perp()

        self.max_speed = self.scale * uniform(10,15)

        self.max_force = 500.0 

        self.path = Path()
        self.randomise_path()

        self.waypoint_threshold = self.scale * 1
        self.panic_radius       = self.scale * 20

        self.wander_target = Vector2D(1,0)
        self.wander_dist = 15 * scale
        self.wander_radius = 2 * scale
        self.wander_jitter = 20 * scale
        self.bRadius = 2 * scale

        self.show_info = False
        

        self.color = 'YELLOW'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]
        self.force = Vector2D()

        self.is_hunter = is_hunter

        if self.is_hunter:
            self.max_speed = self.scale * 10
            self.color = 'RED'
            self.vehicle_shape = [
                Point2D(-2.0,  1.5),
                Point2D( 1.0,  0.0),
                Point2D(-2.0, -1.5),
                Point2D(-1.0,  0.0)
            ]

    # ---

    def render(self):
        egi.set_pen_color(name=self.color)
        points = self.world.transform_points(
            self.vehicle_shape,
            self.pos,
            self.heading,
            self.side,
            Vector2D(self.scale, self.scale)
        )
        egi.closed_shape(points)
        
        if self.mode == 'wander':
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)

            egi.green_pen()
            egi.circle(wld_pos, 10)

            egi.red_pen()
            wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            egi.circle(wld_pos, 3)

        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # vel
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)
     
    # ---

    def update(self, delta):
        force = self.calculate(delta)
        force.truncate(self.max_force)
        self.accel = force / self.mass
        self.vel += self.accel * delta
        self.vel.truncate(self.max_speed)
        self.pos += self.vel * delta
        if self.vel.length_sq() > 0.00001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        self.world.wrap_around(self.pos)

    # ---

    def calculate(self, delta):
        # Returns the updated accel based on the current behaviour
        if self.mode == 'Wander':
            self.force = self.wander(delta)
            return self.force
        elif self.mode == 'Seek':
            self.force =self.seek(self.world.target)
            return self.force
        elif self.mode == 'Arrive':
            self.force = self.arrive(self.world.target)
            return self.force
        elif self.mode == 'Flee':
            self.force = self.flee(self.world.target)
            return self.force
        elif self.mode == 'Follow path':
            self.force = self.follow_path()
            return self.force
        elif self.mode == 'Hide':
            self.force = self.hide(self.world.hunter.pos, delta)
            return self.force
        else:
            return Vector2D()

    # ---

    def wander(self, delta):
        # Generate random jitter
        wt = self.wander_target
        jitter_tts = self.wander_jitter * delta
        wt += Vector2D(uniform(-1, 1) * jitter_tts, uniform(-1, 1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius

        target = wt + Vector2D(self.wander_dist, 0)
        # project the target into world space
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        # and steer towards it
        return self.seek(wld_target)


    # ---

    def seek(self, target_pos):
        # Move toward target_pos
        desired_vel = Vector2D()
        to_target = target_pos - self.pos
        if to_target.length() > self.waypoint_threshold:
            desired_vel = to_target.normalise() * self.max_speed
        return desired_vel - self.vel

    # ---

    def arrive(self, target_pos):
        # Move toward target pos, slowing on arrival
        to_target = target_pos - self.pos
        deceleration_threshold = self.waypoint_threshold * 10
        if to_target.length() < deceleration_threshold:
            # Slow down depending on how close the agent is to target_pos
            decelspeed = to_target.length() / deceleration_threshold
            desired_vel     = to_target.normalise() * self.max_speed * decelspeed
            return desired_vel - self.vel
        else:
            return self.seek(target_pos)

    # ---

    def flee(self, hunter_pos):
        # Move away from hunter_pos
        to_hunter = self.pos - hunter_pos
        if to_hunter.length() < self.panic_radius - self.scale:
            # Speed up depending on how close the agent is to hunter_pos
            proximity_multiplier = (self.panic_radius - to_hunter.length()) / self.panic_radius
            desired_vel     = to_hunter.normalise() * self.max_speed * proximity_multiplier
            return desired_vel - self.vel
        else:
            return Vector2D()

    # ---

    def follow_path(self):
        #
        if self.path.is_finished():
            return self.arrive(self.path.current_pt())
        else:
            if (self.path.current_pt() - self.pos).length() < self.waypoint_threshold:
                self.path.inc_current_pt()
            return self.seek(self.path.current_pt())

    # ---

    def randomise_path(self):
        cx = self.world.cx
        cy = self.world.cy
        margin = min(cx, cy) * 0.2
        self.path.create_random_path(8, margin, margin, cx - margin, cy - margin)

    # ---

       # ---

   
    
  

    def hide(self, hunter_pos, delta):

        to_hunter = self.pos - hunter_pos
       

        best_hiding_spot = None;
        best_hiding_distance = None;

        for object_pos in self.world.objects:

            current_hiding_spot     = self.get_hiding_spot(hunter_pos, Vector2D(object_pos.x, object_pos.y))
            current_hiding_distance = self.pos.distance_sq(current_hiding_spot)
            if not best_hiding_spot or current_hiding_distance < best_hiding_distance:
                best_hiding_spot     = current_hiding_spot
                best_hiding_distance = current_hiding_distance

            egi.green_pen()
            egi.cross(current_hiding_spot, 10)

        if best_hiding_spot:
            return self.arrive(best_hiding_spot)

        return self.flee(hunter_pos)

    # ---

    def get_hiding_spot(self, hunter_pos, object_pos):

        radius = 40
        boundary = 50
        distance = radius + boundary

        return object_pos + (object_pos - hunter_pos).normalise() * distance
