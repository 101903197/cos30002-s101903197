'''Autonomous Agent Movement: Seek, Arrive and Flee

Created for COS30002 AI for Games, Lab 05
By Clinton Woodward cwoodward@swin.edu.au

'''
from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D, Point2D
from world import World
from agent import Agent, AGENT_MODES  # Agent with seek, arrive, flee and pursuit


def on_mouse_motion(x, y, dx, dy):
    world.target = Vector2D(x, y)


def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        world.paused = not world.paused
    elif symbol == KEY.A:
            world.agents.append(Agent(world))
    elif symbol in AGENT_MODES:
        for agent in world.agents:
            if not agent.is_hunter:
                agent.mode = AGENT_MODES[symbol]
    elif symbol == KEY.I:
        for agent in world.agents:
            agent.show_info = not agent.show_info        


                
def on_resize(cx, cy):
    world.cx = cx
    world.cy = cy


if __name__ == '__main__':

    # create a pyglet window and set glOptions
    win = window.Window(width=1600,height=1000, vsync=True, resizable=True)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # needed so that egi knows where to draw
    egi.InitWithPyglet(win)
    # prep the fps display
    fps_display = clock.ClockDisplay()
    # register key and mouse event handlers
    win.push_handlers(on_key_press)
    win.push_handlers(on_mouse_motion)
    win.push_handlers(on_resize)

    # Create the world
    world = World(500, 500)

    # Add the hunter
    world.hunter = Agent(world, 30, 'Wander', True)
    world.agents.append(world.hunter)

    # Add the objects
    world.objects = [
      
        Point2D(1080, 350),
        Point2D( 430, 720),
        Point2D( 520, 130),
        Point2D( 220, 530)
    ]

    # Add some agent
    world.agents.append(Agent(world))

    # unpause the world ready for movement
    world.paused = False

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render()
        fps_display.draw()
        # swap the double buffer
        win.flip()

