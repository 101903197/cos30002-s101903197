from utils.vector2d import *
from utils.graphics import graphics, KEY
import time
import pygame
import random



class GameObject:

    def __init__(self, x, y):

        self.world = None
        self.active = True
        self.pos = Vector2D(x,y)
        self.color = 'WHITE'
        self.radius = 5

        self.vel = Vector2D()
        self.previous_pos = self.pos

    

    def update(self):
        self.vel = self.pos - self.previous_pos
        self.previous_pos = self.pos.copy()

    

    def draw(self):

        graphics.set_pen_color(name = self.color)
        graphics.set_stroke(2)
        graphics.circle(self.pos, self.radius)



class Agent(GameObject):

    weapon = 'Rifle'
    predictive_aiming = True

    weapon_types = {
        KEY._1: 'Rifle',
        KEY._2: 'Rocket',
        KEY._3: 'Handgun',
        KEY._4: 'Grenade'
    }

    weapon_cooldown = {
        'Rifle':   1.0,
        'Rocket':  2.0,
        'Handgun': 1.5,
        'Grenade': 2.0
    }

    def __init__(self, x, y):
        GameObject.__init__(self, x, y)

        self.color = 'YELLOW'
        self.radius = 20

        self.speed = 3
        self.waypoints = [self.pos.copy()]
        self.current_waypoint = 0

        self.last_fired = time.time()
        self.last_stopped = time.time()

        self.mode = 'Patrol'
        self.mode_attack = 'Shooting'
        self.mode_patrol = 'Walking'

    

    def update(self):

        target = self.get_closest_enemy(200)

        if self.mode == 'Patrol' and target:
            self.mode = 'Attack'
            self.color = 'RED'
        elif self.mode == 'Attack' and not target:
            self.mode = 'Patrol'
            self.color = 'WHITE'

        if self.mode == 'Patrol':
            self.patrol()
        if self.mode == 'Attack':
            self.attack(target)

    def draw(self):
        GameObject.draw(self)

        secondary_mode = self.mode_patrol if self.mode == 'Patrol' else self.mode_attack

        graphics.white_pen()
        graphics.text_at_pos(self.pos.x - 20, self.pos.y + 40, self.mode)
        graphics.text_at_pos(self.pos.x - 20, self.pos.y + 25, '(' + secondary_mode + ')')

    def patrol(self):

        if self.mode_patrol == 'Stopped' and time.time() - self.last_stopped > 2:
            self.mode_patrol = 'Walking'

        if self.mode_patrol == 'Walking':
            direction = (self.waypoints[self.current_waypoint] - self.pos).normalise()
            self.pos += direction * self.speed

            if self.pos.distance_sq(self.waypoints[self.current_waypoint]) < 25:
                self.next_waypoint()
                self.last_stopped = time.time()
                self.mode_patrol = 'Stopped'

    def attack(self, target):

        if self.mode_attack == 'Reloading' and time.time() - self.last_fired > Agent.weapon_cooldown[Agent.weapon]+0.5:
            self.mode_attack = 'Shooting'

        if self.mode_attack == 'Shooting':

            if time.time() - self.last_fired > Agent.weapon_cooldown[Agent.weapon]:
                self.fire(target)
                self.last_fired = time.time()

            if time.time() - self.last_fired > Agent.weapon_cooldown[Agent.weapon] / 2:
                self.mode_attack = 'Reloading'


    def add_waypoint(self, x, y):

        self.waypoints.append(Vector2D(x,y))

    def next_waypoint(self):

        self.current_waypoint += 1
        if self.current_waypoint >= len(self.waypoints):
            self.current_waypoint = 0

    def get_closest_enemy(self, in_radius):

        enemies = list(filter(lambda game_object : isinstance(game_object, Enemy), self.world.game_objects))
        if not enemies:
            return None
        target = min(enemies, key = lambda enemy : self.pos.distance_sq(enemy.pos))
        if self.pos.distance_sq(target.pos) > pow(in_radius, 2):
            return None
        return target


    def get_predicted_pos(self, enemy_pos, enemy_vel, projectile_speed):

        estimated_time = (enemy_pos - self.pos).length() / projectile_speed
        return enemy_pos + enemy_vel * estimated_time

    

    def fire(self, enemy):

        target_pos = enemy.pos

        if Agent.predictive_aiming:
            projectile_speed = 20 if self.weapon in ['Rifle', 'Handgun'] else 10
            target_pos = self.get_predicted_pos(enemy.pos, enemy.vel, projectile_speed)

        if Agent.weapon == 'Rifle':
            self.world.add(Riflegun(self.pos, target_pos))

        elif Agent.weapon == 'Rocket':
            self.world.add(Rocket(self.pos, target_pos))

        elif Agent.weapon == 'Handgun':
            self.world.add(Handgunbullet(self.pos, target_pos))

        elif Agent.weapon == 'Grenade':
            self.world.add(Grenade(self.pos, target_pos))



class Projectile(GameObject):

    def __init__(self, pos, target_pos):
        GameObject.__init__(self, pos.x, pos.y)
        self.direction = (target_pos - pos).normalise()
        self.speed = 5

    

    def update(self):

        self.pos += self.direction * self.speed
        if not pygame.Rect(0, 0, self.world.width, self.world.height).collidepoint(self.pos.x, self.pos.y):
            self.active = False
            return
        for enemy in list(filter(lambda game_object: isinstance(game_object, Enemy), self.world.game_objects)):
            if (self.pos - enemy.pos).length_sq() < pow(self.radius + enemy.radius, 2):
                self.active = False
                enemy.active = False
                return
        GameObject.update(self)



class Riflegun(Projectile):

    def __init__(self, pos, target_pos):
        Projectile.__init__(self, pos, target_pos)

        self.color = 'YELLOW'
        self.radius = 5
        self.speed = 20



class Rocket(Projectile):

    def __init__(self, pos, target_pos):
        Projectile.__init__(self, pos, target_pos)

        self.color = 'ORANGE'
        self.radius = 15
        self.speed = 10



class Handgunbullet(Projectile):

    def __init__(self, pos, target_pos):
        target_pos += Vector2D(
            random.randrange(-100, 100),
            random.randrange(-100, 100)
        )
        Projectile.__init__(self, pos, target_pos)

        self.color = 'BLUE'
        self.radius = 5
        self.speed = random.randrange(18, 22)



class Grenade(Projectile):

    def __init__(self, pos, target_pos):
        target_pos += Vector2D(
            random.randrange(-50, 50),
            random.randrange(-50, 50)
        )
        Projectile.__init__(self, pos, target_pos)

        self.color = 'GREEN'
        self.radius = 10
        self.speed = 10



class Enemy(GameObject):

    def __init__(self, x, y):
        GameObject.__init__(self, x, y)

        self.speed = 10

        self.color = 'RED'
        self.radius = 15

    

    def update(self):
        if (self.world.mouse_pos - self.pos).length_sq() > pow(self.radius, 2):
            self.pos += (self.world.mouse_pos - self.pos).normalise() * self.speed
        GameObject.update(self)







