from pyglet.gl import *
from GameObject import *
from World import World
from utils.graphics import KEY



world = World(1400, 1000)
agent = Agent(400,400)
agent.add_waypoint(1000, 700)
agent.add_waypoint(1000, 300)
agent.add_waypoint(300, 700)
agent.add_waypoint(500, 400)
agent.add_waypoint(1000, 600)
world.add(agent)
world.add(Enemy(800,700))



def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        Agent.predictive_aiming = not Agent.predictive_aiming
    elif symbol in Agent.weapon_types:
        Agent.weapon = Agent.weapon_types[symbol]



def on_mouse_motion(x, y, dx, dy):

    world.mouse_pos = Vector2D(x, y)



def on_mouse_press(x, y, button, modifiers):

    world.add(Enemy(x,y))



window = pyglet.window.Window(
    width  = 1200,
    height = 800,
    vsync     = True,
    resizable = True
)



glEnable(GL_BLEND)
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
graphics.InitWithPyglet(window)

window.push_handlers(on_key_press)
window.push_handlers(on_mouse_motion)
window.push_handlers(on_mouse_press)

while not window.has_exit:

    window.dispatch_events()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    world.update()
    world.draw()
    window.flip()

