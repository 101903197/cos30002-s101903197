2019 AI for Games
Who Am I?
TODO: Add your name, student id, and easy email link here, formatted as a unordered list.

Name : Saikiran Solanki

Student ID: 101903197

Email: 101903197@student.swin.edu

Purpose
Purpose of learning this unit is to gain knowledge of the world of Artificial Intelligence. Learning it would be very useful for my future progress in the technological field.

Anything Else?
TODO: You can use this file as a quick way to get to useful locations in your repository, links to resources/notes and other handy details. It is here for you.

TODO: You can delete all the text we have put here to start with, but keep the essential "who am I" details in some form.

A useful "README" file is considered a good practice for shared or world-visible repositories, so it makes sense to get into the habit for this unit. However, please keep in mind that this repository should NOT be made visible to the world.

Notes
Do not share teaching materials provided in this unit online. That include lecture notes, sample code, spike documents, as well as your own work that could be inappropriately used by someone else.
TODO: Learn to use markdown! There are many introduction and tutorials to markdown. Here is one https://bitbucket.org/tutorials/markdowndemo/src/master/
bitbucket's web interface presents markdown in some fancy ways, including nice formatting of many common programming language. Can you take advantage of this?
About Not Sharing Unit Materials and Your Work Publically.
You may think that asking you not to share teaching resources online, and not sharing your own work, is restrictive. However, we require this to protect both your work from misuse, as well as to limit issues with teaching materials being shared in an uncontrolled way.
