from random import choice 
class Simplebot(object):
    def update(self, gameinfo):
        # check if we should attack
        if gameinfo.my_planets and gameinfo.not_my_planets:
            # launch new fleet if there's enough ships
            dest = choice(list(gameinfo.not_my_planets.values()))
            src = choice(list(gameinfo.my_planets.values()))
            if src.num_ships > 30:  
                gameinfo.planet_order(src, dest, int(src.num_ships * 0.75) )  
            dest = min(gameinfo.not_my_planets.values(),key=lambda p: 1.0 / (1 + p.num_ships))
