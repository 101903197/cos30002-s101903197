import random

#Three state machine
#States I have used : Defending, Attacking, Healing

states = ['defending','attacking','healing']
current_state = 'attacking'

health=200
tower_health=450
special_tower_heal=random.randint(20,80)

alive= True
game_rounds=20
current_round=0

while alive and (current_round<20):
    current_round+=1
    
    print("Soldier health = "+str(health))
    print("Tower health = "+str(tower_health))
    print("Current Round = "+str(current_round)+"\n")

    if current_state is 'attacking':
        print("Attaaaaacccckkkkk!\n")
        health-=40;
        tower_health-=35
        if(health<30 and tower_health>0):
            current_state='healing'
        elif(health>30 and tower_health<300):
            current_state='defending'
        elif(health>30 and tower_health>300):
            current_state='attacking'
        else:
            print("Nooooo... I dont wanna die !\n")
            alive=False
    elif current_state is 'defending':
        print("I will defend tower by any means !\n")
        health-=30;
        tower_health-=10;
        if(health<30 and tower_health>0):
            current_state='healing'
        elif(health>30 and tower_health>300):
            current_state='attacking'
        elif(health>30 and tower_health<300):
            current_state='defending'    
        else:
            print("Nooooo... I dont wanna die !\n")
            alive=False
    elif current_state is 'healing':
        health+=85
        print("You have been healed Soldier! \n")
        if(random.randint(1,3)==2):
            tower_health+=special_tower_heal
            print("Your tower was miracuously healed by "+str(special_tower_heal))
       
        if(health>30 and tower_health<300):
            current_state='defending'
        elif(health>30 and tower_health>0):
            current_state='attacking'
        else:
            print("Nooooo... I dont wanna die !\n")
            alive=False
        special_tower_heal=random.randint(20,80)
    else:
        print("Hey Whatcha Doing here ? \nMy basic game is working perfectly fine mate.;p\n")

    
            
          
print("You played very well. Have a good day brave soldier.\n")        
