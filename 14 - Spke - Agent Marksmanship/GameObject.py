from utils.vector2d import *
from utils.graphics import graphics, KEY
import time
import pygame
import random



class GameObject:

    def __init__(self, x, y):

        self.world = None
        self.active = True
        self.pos = Vector2D(x,y)
        self.color = 'WHITE'
        self.radius = 5

        self.vel = Vector2D()
        self.previous_pos = self.pos

    

    def update(self):
        self.vel = self.pos - self.previous_pos
        self.previous_pos = self.pos.copy()

    

    def draw(self):

        graphics.set_pen_color(name = self.color)
        graphics.set_stroke(2)
        graphics.circle(self.pos, self.radius)



class Agent(GameObject):

    weapon = 'Rifle'
    predictive_aiming = True

    weapon_types = {
        KEY._1: 'Rifle',
        KEY._2: 'Rocket',
        KEY._3: 'Handgun',
        KEY._4: 'Grenade'
    }

    weapon_cooldown = {
        'Rifle':   0.5,
        'Rocket':  2.0,
        'Handgun': 1.0,
        'Grenade': 1.5
    }

    def __init__(self, x, y):
        GameObject.__init__(self, x, y)

        self.color = 'YELLOW'
        self.radius = 20

        self.last_fired = time.time() - Agent.weapon_cooldown[Agent.weapon]

    

    def update(self):

        if time.time() - self.last_fired > Agent.weapon_cooldown[Agent.weapon]:
            self.last_fired = time.time()
            enemies = list(filter(lambda game_object : isinstance(game_object, Enemy), self.world.game_objects))
            if not enemies:
                return
            target = min(enemies, key = lambda enemy : self.pos.distance_sq(enemy.pos))
            self.fire(target)

    

    def get_predicted_pos(self, enemy_pos, enemy_vel, projectile_speed):

        estimated_time = (enemy_pos - self.pos).length() / projectile_speed
        return enemy_pos + enemy_vel * estimated_time

    

    def fire(self, enemy):

        target_pos = enemy.pos

        if Agent.predictive_aiming:
            projectile_speed = 20 if self.weapon in ['Rifle', 'Handgun'] else 10
            target_pos = self.get_predicted_pos(enemy.pos, enemy.vel, projectile_speed)

        if Agent.weapon == 'Rifle':
            self.world.add(Riflegun(self.pos, target_pos))

        elif Agent.weapon == 'Rocket':
            self.world.add(Rocket(self.pos, target_pos))

        elif Agent.weapon == 'Handgun':
            self.world.add(Handgunbullet(self.pos, target_pos))

        elif Agent.weapon == 'Grenade':
            self.world.add(Grenade(self.pos, target_pos))



class Projectile(GameObject):

    def __init__(self, pos, target_pos):
        GameObject.__init__(self, pos.x, pos.y)
        self.direction = (target_pos - pos).normalise()
        self.speed = 5

    

    def update(self):

        self.pos += self.direction * self.speed
        if not pygame.Rect(0, 0, self.world.width, self.world.height).collidepoint(self.pos.x, self.pos.y):
            self.active = False
            return
        for enemy in list(filter(lambda game_object: isinstance(game_object, Enemy), self.world.game_objects)):
            if (self.pos - enemy.pos).length_sq() < pow(self.radius + enemy.radius, 2):
                self.active = False
                enemy.active = False
                return
        GameObject.update(self)



class Riflegun(Projectile):

    def __init__(self, pos, target_pos):
        Projectile.__init__(self, pos, target_pos)

        self.color = 'YELLOW'
        self.radius = 5
        self.speed = 20



class Rocket(Projectile):

    def __init__(self, pos, target_pos):
        Projectile.__init__(self, pos, target_pos)

        self.color = 'ORANGE'
        self.radius = 15
        self.speed = 10



class Handgunbullet(Projectile):

    def __init__(self, pos, target_pos):
        target_pos += Vector2D(
            random.randrange(-100, 100),
            random.randrange(-100, 100)
        )
        Projectile.__init__(self, pos, target_pos)

        self.color = 'BLUE'
        self.radius = 5
        self.speed = random.randrange(18, 22)



class Grenade(Projectile):

    def __init__(self, pos, target_pos):
        target_pos += Vector2D(
            random.randrange(-50, 50),
            random.randrange(-50, 50)
        )
        Projectile.__init__(self, pos, target_pos)

        self.color = 'GREEN'
        self.radius = 10
        self.speed = 10



class Enemy(GameObject):

    def __init__(self, x, y):
        GameObject.__init__(self, x, y)

        self.speed = 10

        self.color = 'RED'
        self.radius = 15

    

    def update(self):
        if (self.world.mouse_pos - self.pos).length_sq() > pow(self.radius, 2):
            self.pos += (self.world.mouse_pos - self.pos).normalise() * self.speed
        GameObject.update(self)







